# timwcap_hackathon
# Hermes

Hermes, abbatte le barriere "alla velocità del suono", con un sistema di traduzione istantanea integrato alla possibilità di avviare e partecipare a videoconferenze online con trascrizione testuale.

## Getting Started

Per poter provare tale progetto è necessario utilizzare l'ide Android Studio con Flutter installato ed aggiornato sulla macchina in utilizzo.
Al fine di poter utilizzare tale progetto bisogna navigare al seguente file /lib/main.dart e sostituire le API KEY nella rispettiva variabile.

