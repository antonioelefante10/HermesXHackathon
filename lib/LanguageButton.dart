import 'package:flutter/material.dart';

class LanguageButton extends StatefulWidget {


  LanguageButton({this.accessibleView,this.primaryColor, this.secondaryColor,this.tertiaryColor,this.text,this.pressed});

  bool accessibleView;
  Color primaryColor;
  Color secondaryColor;
  Color tertiaryColor;
  bool pressed = false;

  String text;

  @override
  _LanguageButtonState createState() => _LanguageButtonState();
}

class _LanguageButtonState extends State<LanguageButton> {


  double width = 80;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Text(widget.text,style: TextStyle(
          color: !widget.accessibleView ? widget.tertiaryColor : !this.widget.pressed ? Colors.black : Colors.white,
          fontSize: 12
      ),
      ),
      alignment: Alignment.center,
      width: width,
      height: 60,
      decoration: !widget.accessibleView ? BoxDecoration(
          color: !this.widget.pressed ? widget.primaryColor : widget.secondaryColor,
          borderRadius: BorderRadius.circular(width/4),
          boxShadow: [
            BoxShadow(
              color: Color(0xFF143C77),
              offset: Offset(
                  width/3, width/3),
              blurRadius: width/3,
            ),
            BoxShadow(
              color: Color(0xFF1A51A1),
              offset: Offset(
                  -width/3, -width/3),
              blurRadius: width*2,
            ),

          ]) : BoxDecoration(
        color: !this.widget.pressed ? widget.tertiaryColor : widget.secondaryColor,
        borderRadius: BorderRadius.circular(width/4),
      ),
    );
  }
}
