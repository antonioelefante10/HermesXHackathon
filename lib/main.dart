import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:Hermes/LanguageButton.dart';
import 'package:audioplayer/audioplayer.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:path_provider/path_provider.dart';
import 'package:recorder_wav/recorder_wav.dart';
import 'package:translator/translator.dart';

/*
    REST API URLs
*/
final speechToTextURL =
    "https://hackathon.tim.it/gcloudspeechtotext/v1/speech:longrunningrecognize";
final speechToTextGetURL =
    "https://hackathon.tim.it/gcloudspeechtotext/v1/operations/";
final textToSpeechURL =
    "https://hackathon.tim.it/gcloudtexttospeech/v1/text:synthesize?";

/*
    inserisci qui le API Key fornite dal server TIM
 */
final apiKey = "API KEY";

final primaryColor = Color(0xFF17468C);
final secondaryColor = Color(0xFFD92E34);
final tertiaryColor = Color(0xFFFFFFFF);

DateTime recordingStart;
DateTime recordingStop;

Future<void> main() async {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

  // Audio Player per poter permettere la riproduzione del contenuto estrapolato dalle API TextToSpeech
  AudioPlayer audioPlugin = AudioPlayer();

  // Variabili booleane che definiscono lo stato dell'applicazione.
  bool _isRecording = false;
  bool _isPlaying = false;
  bool _isLoading = false;

  /*
     Il design del prototipo si ispira ad uno stile molto in voga del 2020, il Neumorfismo, tuttavia per quanto "Soffice" e bello
     tale stile grafico sia, purtroppo risulta essere poco accessibile a chi ha difficoltà visive. Per tale motivazione
     anche se è un prototipo abbiamo deciso di inserire uno switch per attivare e disattivare la modalità di accessibilità
     così da proporre colori ad alto contrasto pur non perdendo la cura che noi abbiamo deciso di proporre all'interno di questo prodotto
    */
  bool _accessibleView = false;



  /*
  Il contenuto sarà quindi visualizzato mediante l'utilizzo di tale variabile che si occuperà di mantenere il testo estrapolato
  oltre che la traduzione ottenuta.
   */

  String recognizedText = "Testo riconosciuto";

/*
Tali variabili sono utilizzate per mantenere la lingua selezionata, così da poter tradurre il contenuto in diverse lingue.
 */
  String _selectedLanguage = "Russo";
  String _languageCodeShort = "ru";
  String _languageCode = "ru-RU";

  /*
  Tale oggetto mi permette di effettuare una traduzione mediante l'utilizzo di Google Translate.
   */
  final translator = GoogleTranslator();

  String filePath =
      "this variable will be overwritten by the path to a temporary directory";

  final headers = {"apikey": apiKey};

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
          backgroundColor: primaryColor,
          appBar: AppBar(
            title: const Text('Hermes Proof Of Concept'),
            backgroundColor: primaryColor,
            shadowColor: Color(0x00000000),
          ),
          body: Builder(
            builder: (context) {
              final width = MediaQuery.of(context).size.width;
              return Stack(
                children: [
                  Center(
                      child: Column(
                    children: [
                      Spacer(flex: 1,),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          GestureDetector(
                            child: LanguageButton(accessibleView: _accessibleView,text: "Russo",primaryColor: primaryColor, secondaryColor: secondaryColor,tertiaryColor: tertiaryColor,pressed: _selectedLanguage == "Russo"),
                            onTap: () {
                              setState(() {
                                _selectedLanguage = "Russo";
                                _languageCodeShort = "ru";
                                _languageCode = "ru-RU";
                              });
                            },
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          GestureDetector(
                            child: LanguageButton(accessibleView: _accessibleView,text: "Inglese",primaryColor: primaryColor, secondaryColor: secondaryColor,tertiaryColor: tertiaryColor,pressed: _selectedLanguage == "Inglese" ),
                            onTap: () {
                              setState(() {
                                _selectedLanguage = "Inglese";
                                _languageCodeShort = "en";
                                _languageCode = "en-EN";
                              });
                            },
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          GestureDetector(
                            child: LanguageButton(accessibleView: _accessibleView,text: "Spagnolo",primaryColor: primaryColor, secondaryColor: secondaryColor,tertiaryColor: tertiaryColor,pressed: _selectedLanguage == "Spagnolo"),
                            onTap: () {
                              setState(() {
                                _selectedLanguage = "Spagnolo";
                                _languageCodeShort = "es";
                                _languageCode = "es-419";
                              });
                            },
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                _selectedLanguage = "Giapponese";
                                _languageCodeShort = "ja";
                                _languageCode = "ja-JP";
                              });
                            },
                            child: LanguageButton(accessibleView: _accessibleView,text: "Giapponese",primaryColor: primaryColor, secondaryColor: secondaryColor,tertiaryColor: tertiaryColor,pressed: _selectedLanguage == "Giapponese"),
                          ),
                        ],
                      ),
                      SizedBox(height: 40,),
                      GestureDetector(
                        onTap: () {
                          if(!_isLoading && !_isRecording && !_isPlaying) {
                            if(!_isRecording) {
                              startSession();
                            }

                          }
                          else if (_isRecording) {
                            stopRecording();
                          }
                        },
                        child: Container(
                          height: width * 0.6,
                          width: width * 0.6,
                          decoration: !_accessibleView ? BoxDecoration(
                              color: primaryColor,
                              borderRadius: BorderRadius.circular(width * 0.1),
                              boxShadow: [
                                BoxShadow(
                                  color: Color(0xFF143C77),
                                  offset: Offset(
                                      width * 0.1 * 0.333, width * 0.1 * 0.333),
                                  blurRadius: width * 0.1,
                                ),
                                BoxShadow(
                                  color: Color(0xFF1A51A1),
                                  offset: Offset(-width * 0.1 * 0.333,
                                      -width * 0.1 * 0.333),
                                  blurRadius: width * 0.1,
                                ),
                              ]) : BoxDecoration(
                            color: tertiaryColor,
                            borderRadius: BorderRadius.circular(width * 0.1),
                          ),
                          child: _isPlaying ? Icon(
                            Icons.queue_music,
                            color: secondaryColor,
                            size: width * 0.3,
                          ) : _isLoading ? Icon(
                            Icons.cloud_upload,
                            color: secondaryColor,
                            size: width * 0.3,
                          ) : !_isRecording
                              ? Icon(
                            Icons.keyboard_voice,
                            color: secondaryColor,
                            size: width * 0.3,
                          )
                              : Icon(
                            Icons.record_voice_over,
                            color: secondaryColor,
                            size: width * 0.3,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 25,
                      ),
                      Container(
                        width: width * 0.8,
                        alignment: Alignment.center,
                        child: Text(
                          recognizedText,
                          style: TextStyle(color: tertiaryColor),
                        ),
                      ),
                      Spacer(flex: 3,),
                    ],
                  )),
                  Positioned(
                    bottom: 36,
                    left: width * 0.4,
                    right: width * 0.4,
                    child: GestureDetector(
                      onTap: () {
                        setState(() {
                          _accessibleView = !_accessibleView;
                        });
                      },
                      child: Container(
                        height: width - 2 * width * 0.4,
                        decoration: !_accessibleView
                            ? BoxDecoration(
                                color: tertiaryColor,
                                borderRadius:
                                    BorderRadius.circular(50 * 0.1),
                                boxShadow: [
                                    BoxShadow(
                                      color: Color(0xFF143C77),
                                      offset: Offset(50 * 0.1 * 0.333,
                                          50 * 0.1 * 0.333),
                                      blurRadius: 50 * 0.1,
                                    ),
                                    BoxShadow(
                                      color: Color(0xFF1A51A1),
                                      offset: Offset(-50 * 0.1 * 0.333,
                                          -50 * 0.1 * 0.333),
                                      blurRadius: 50 * 0.1,
                                    ),
                                  ])
                            : BoxDecoration(
                          color: tertiaryColor,
                          borderRadius:
                          BorderRadius.circular(50 * 0.1),
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(
                              Icons.accessibility_new,
                              size: _accessibleView ? 32 : 42,
                              color: primaryColor,
                            ),
                            _accessibleView ? Text("Accessibile!",
                            style: TextStyle(
                              color: primaryColor,
                              fontSize: 12
                            ),) : Container()
                          ],
                        ),
                      ),
                    ),
                  )
                ],
              );
            },
          )),
    );
  }


  void startSession() async {
    final appDocDirectory = await getApplicationDocumentsDirectory();
    filePath = '/flutter_audio_recorder_';
    filePath = appDocDirectory.path +
        filePath +
        DateTime.now().millisecondsSinceEpoch.toString();
    recordingStart = DateTime.now();
    RecorderWav.startRecorder();
    setState(() {
      _isRecording = true;
    });
  }


  /* Il completo funzionamento del prototipo si trova in questa funzione, la quale una volta terminato
  di estrarre il contenuto vocale, questo viene convertito in base64 come richiesto dalle API (poichè JSON
  non permette di inserire contenuto binario) e trasferito mediante REST API Call al server il quale eseguirà
  le elaborazioni richieste. Una volta fatto ciò il software dovrebbe attendere che l'estrazione del testo dal
  contenuto audio sia effettuato (in questo prototipo non sono stati effettuati check sugli errori, ne casi di test,
   per ragioni di tempo).
   Una volta fatto ciò questo viene tradotto mediante Google Translate ed inviato nuovamente ad i server Tim per
   poter sintetizzare un contenuto audio a partire dal testo fornito. Questo successivamente verrà riprodotto
   sul dispositivo locale.
   */
  void stopRecording() async {
    setState(() {
      _isRecording = false;
    });
    filePath = await RecorderWav.StopRecorder();
    recordingStop = DateTime.now();
    final bytes = await File(filePath).readAsBytes();
    final extractedText = await speechToText(base64Data: base64Encode(bytes));
    setState(() {
      recognizedText = extractedText;
    });
    final translatedText = await translate(extractedText: extractedText, languageCode: _languageCodeShort);
    setState(() {
      recognizedText += "\n" + translatedText;
    });
    await textToSpeech(text: translatedText,languageCode: _languageCode);
    playSound();
  }

  Future<String> speechToText({String base64Data, String languageCode = 'it-IT'}) async {
    setState(() {
      _isLoading = true;
    });
    final body = {
      "config": {
        "languageCode": languageCode,
        "encoding": "LINEAR16",
        "sampleRateHertz": 16000
      },
      "audio": {"content": base64Data}
    };
    //encode Map to JSON
    var encoded = json.encode(body);

    var response =
        await http.post(speechToTextURL, headers: headers, body: encoded);
    final responseDecoded = json.decode(response.body);
    final resourceName = responseDecoded['name'];
    await Future.delayed(Duration(seconds: recordingStop.difference(recordingStart).inSeconds));
    response =
        await http.get(speechToTextGetURL + resourceName, headers: headers);
    final responseDec = json.decode(response.body);
    print(responseDec);
    final extractedText =
        responseDec['response']['results'][0]['alternatives'][0]['transcript'];
    return extractedText;
  }

  Future<String> translate(
      {String extractedText, String languageCode = 'ru'}) async {
    var translation =
        await translator.translate(extractedText, to: languageCode);
    return translation;
  }

  Future<void> textToSpeech({String text, String languageCode = 'ru-RU'}) async {
    final body = {
      "input": {"text": text},
      "voice": {"languageCode": languageCode},
      "audioConfig": {"audioEncoding": "MP3", "sampleRateHertz": 16000}
    };
    //encode Map to JSON
    var encodedBody = json.encode(body);

    final response =
        await http.post(textToSpeechURL, headers: headers, body: encodedBody);
    final jsonEncodedResponse = json.decode(response.body);
    final receivedSound = jsonEncodedResponse['audioContent'];
    await RecorderWav.removeRecorderFile(filePath);
    final directory = await getApplicationDocumentsDirectory();
    filePath = directory.path + DateTime.now().millisecondsSinceEpoch.toString() + "received_sound.mp3";
    final decodedReceivedSound = base64.decode(receivedSound);
    await File(filePath).writeAsBytes(decodedReceivedSound);
  }

  Future<void> playSound() async {
    audioPlugin.stop();
    audioPlugin.play(filePath, isLocal: true);
    Future.delayed(audioPlugin.duration).then((value) {
      File(filePath).delete().then((value) => null);
      setState(() {
        _isPlaying = false;
        _isRecording = false;
        _isLoading = false;
      });
    });
    setState(() {
      _isPlaying = true;
    });
  }
}
